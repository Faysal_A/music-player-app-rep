package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateDataModel();
        connectXMLViews();
        setUpRecyclerView();
        displayCurrentSong();

    }

    void populateDataModel(){
        //Initializing first playlist
        firstPlaylist.name = "First Playlist";
        firstPlaylist.songs = new ArrayList<Song>();

        //Creating and initializing first song
        Song song = new Song("Acoustic Breeze", "bensound.com", R.raw.acousticbreeze, R.drawable.acousticbreeze);

        //Adding first song to first playlist using arraylist of song
        firstPlaylist.songs.add(song);

        song = new Song("A new beginning", "bensound.com", R.raw.anewbeginning, R.drawable.anewbeginning);
        firstPlaylist.songs.add(song);

        song = new Song("Creative minds", "bensound.com", R.raw.creativeminds, R.drawable.creativeminds);
        firstPlaylist.songs.add(song);

        song = new Song("Going higher", "bensound.com", R.raw.goinghigher, R.drawable.goinghigher);
        firstPlaylist.songs.add(song);

        song = new Song("Happy rock", "bensound.com", R.raw.happyrock, R.drawable.happyrock);
        firstPlaylist.songs.add(song);

        song = new Song("Hey", "bensound.com", R.raw.hey, R.drawable.hey);
        firstPlaylist.songs.add(song);
    }

    void connectXMLViews(){
        recyclerView = findViewById(R.id.songList);
        songImageView = findViewById(R.id.songImage);
        songNameView = findViewById(R.id.songName);
        artistNameView = findViewById(R.id.artistName);
        previous = findViewById(R.id.previous);
        playPause = findViewById(R.id.playPause);
        next = findViewById(R.id.next);

    }

    void setUpRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Connect the adapter to recyclerView
        adapter = new SongAdapter(this, firstPlaylist.songs);
        recyclerView.setAdapter(adapter);
    }

    void displayCurrentSong(){
        Song currentSong  = firstPlaylist.songs.get(currentSongIndex);
        songImageView.setImageResource(currentSong.songImagePath);
        songNameView.setText(currentSong.songName);
        artistNameView.setText(currentSong.artistName);
    }

    public void startMP(View view) {
        mp.start();
    }

    public void pauseMP(View view) {
        mp.pause();
    }

    //Properties
    Playlist firstPlaylist = new Playlist();
    SongAdapter adapter;
    Integer currentSongIndex = 0;

    //XML Views
    MediaPlayer mp;
    RecyclerView recyclerView;
    ImageView songImageView;
    TextView songNameView;
    TextView artistNameView;
    ImageButton previous;
    ImageButton playPause;
    ImageButton next;
}