package com.example.musicplayer;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SongViewHolder extends RecyclerView.ViewHolder {

    //Constructor
    public SongViewHolder(@NonNull View itemView) {
        super(itemView);

        this.itemView = itemView;
        imageView = itemView.findViewById(R.id.smallSongImage);
        songNameTextView = itemView.findViewById(R.id.songNameTextView);
        artistNameTextView = itemView.findViewById(R.id.artistNameTextView);

    }

    //Properties
    View itemView;
    ImageView imageView;
    TextView songNameTextView;
    TextView artistNameTextView;
}
