package com.example.musicplayer;

import java.util.ArrayList;
import java.util.Arrays;

public class Song {

    String songName;
    String artistName;
    int songPath;
    int songImagePath;

        public Song(String sName, String aName, int sPath, int siPath){

            songName = sName;
            artistName = aName;
            songPath = sPath;
            songImagePath = siPath;

        }

}
