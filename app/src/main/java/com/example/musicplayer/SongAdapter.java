package com.example.musicplayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter  extends RecyclerView.Adapter<SongViewHolder> {

    //Constructor
        SongAdapter(@NonNull Context context, @NonNull ArrayList<Song> songs){
            this.context = context;
            this.songs = songs;
        }
    //Overridden methods
    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.song_layout, parent, false);
        SongViewHolder viewHolder = new SongViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {
        //Called when a excisting viewholder needs to be re-used
        //At that point we need to repopulate the viewholder
        Song song = songs.get(position);
        holder.imageView.setImageResource(song.songImagePath);
        holder.songNameTextView.setText(song.songName);
        holder.artistNameTextView.setText(song.artistName);
    }

    @Override
    public int getItemCount() {
            //Called when recyclerview needs to know how many items in total it needs to display
        return songs.size();
    }

    //Properties
    Context context;
    ArrayList<Song> songs;

}
